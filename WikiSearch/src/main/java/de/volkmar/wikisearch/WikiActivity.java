package de.volkmar.wikisearch;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class WikiActivity extends Activity {

    public static final String INTENT_EXTRA_OS_NAME = "INTENT_EXTRA_OS_NAME";

    private WebView wvWiki;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wiki);

        if(!getIntent().hasExtra(INTENT_EXTRA_OS_NAME)) {
            Toast.makeText(this, "Kein Extra gefunden!", Toast.LENGTH_LONG).show();
            finish();
        }

        String strOSName = getIntent().getStringExtra(INTENT_EXTRA_OS_NAME);

        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setTitle(strOSName);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        wvWiki = (WebView) findViewById(R.id.wvWiki);

        wvWiki.setWebViewClient(new WebViewClient());
        wvWiki.loadUrl("http://de.wikipedia.org/wiki/" + strOSName);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
