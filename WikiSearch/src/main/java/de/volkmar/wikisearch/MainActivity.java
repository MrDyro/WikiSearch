package de.volkmar.wikisearch;

import android.R;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class MainActivity extends ListActivity implements AdapterView.OnItemClickListener{

    private String[] straOSNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        straOSNames = new String[] {"Windows", "Android", "iOS", "Linux"};

        setListAdapter(new ArrayAdapter<String>(this, R.layout.simple_list_item_1, R.id.text1, straOSNames));
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        if(!isNetworkAvailable()) {
            Toast.makeText(this, "Kein Internet!", Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(this, WikiActivity.class);
        intent.putExtra(WikiActivity.INTENT_EXTRA_OS_NAME, straOSNames[i]);
        startActivity(intent);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }
}
